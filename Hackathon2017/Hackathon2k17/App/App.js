﻿var app = angular.module('Hackathon2k17', ['ngRoute']);
app.config(function ($routeProvider) {
    $routeProvider
        .when('/Home', {
            controller: 'HomeCtrl',
            templateUrl: 'App/Home/HomeView.html'
        })
        .when('/About', {
            controller: 'AboutCtrl',
            templateUrl: 'App/About/AboutView.html'
        })
        .when('/GoogleMap', {
            controller: 'GoogleMapCtrl',
            templateUrl: 'App/GoogleMap/GoogleMapView.html'
        })
        .when('/Hotline', {
            controller: 'HotlineCtrl',
            templateUrl: 'App/Hotline/HotlineView.html'
        })
        .when('/SpeakOut', {
            controller: 'SpeakOutCtrl',
            templateUrl: 'App/SpeakOut/SpeakOutView.html'
        })
        .when('/Signup', {
            controller: 'SignupCtrl',
            templateUrl: 'App/Signup/SignupView.html'
        })
        .when('/Login', {
            controller: 'LoginCtrl',
            templateUrl: 'App/Login/LoginView.html'
        })
        .when('/ForumRedirect', {
            controller: 'ForumRedirectCtrl',
            templateUrl: 'App/ForumRedirect/ForumRedirect.html'
        })
        .otherwise({
            redirectTo: '/Login'
        });
})